use std::fs;

mod part1;
mod part2;

pub fn day(filename: String) {
    let numbers: Vec<i32> = fs::read_to_string(filename)
        .unwrap()
        .split_whitespace()
        .map(|s| s.parse().unwrap())
        .collect();

    println!("part 1:");
    part1::solve(&numbers); // 1013211

    println!("part 2:");
    part2::solve(&numbers); // 13891280
}
