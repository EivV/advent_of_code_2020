pub fn solve(numbers: &Vec<i32>) {
    for num in numbers.iter() {
        for num2 in numbers.iter() {
            for num3 in numbers.iter() {
                match num + num2 + num3 {
                    2020 => println!("found it! {}", num * num2 * num3),
                    _ => continue,
                }
            }
        }
    }
}
