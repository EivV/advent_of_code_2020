//--- Part Two ---
//
// As you finish the last group's customs declaration, you notice that you misread one word in the
// instructions:
//
// You don't need to identify the questions to which anyone answered "yes"; you need to identify the
// questions to which everyone answered "yes"!
//
// Using the same example as above:
//
// abc
//
// a
// b
// c
//
// ab
// ac
//
// a
// a
// a
// a
//
// b
//
// This list represents answers from five groups:
//
//     In the first group, everyone (all 1 person) answered "yes" to 3 questions: a, b, and c.
//     In the second group, there is no question to which everyone answered "yes".
//     In the third group, everyone answered yes to only 1 question, a. Since some people did not
// answer "yes" to b or c, they don't count.
//     In the fourth group, everyone answered yes to only 1 question, a.
//     In the fifth group, everyone (all 1 person) answered "yes" to 1 question, b.
//
// In this example, the sum of these counts is 3 + 0 + 1 + 1 + 1 = 6.
//
// For each group, count the number of questions to which everyone answered "yes". What is the sum
// of those counts?
use crate::day6::{update_with_more_unique_chars};
use std::collections::{HashMap, HashSet};
use std::collections::hash_map::RandomState;


pub fn solve(inputs: &Vec<String>) {
    let mut counter = 0;
    for group in inputs.into_iter() {
        let persons = group.split("\r\n").map(|s| s.to_string());
        let mut group_answers: Vec<HashMap<char, i32>> = Vec::new();

        let mut all: HashSet<char> = HashSet::new();
        for recount in persons.into_iter() {
            let mut unique_chars: HashMap<char, i32> = HashMap::new();
            update_with_more_unique_chars(&recount, &mut unique_chars);
            group_answers.push(unique_chars);
        }

        for one in group_answers.iter() {
            let keys: HashSet<char> = one.keys().map(|k| *k).collect();
            all = all.union(&keys).map(|k| *k).collect();
        }

        for one in group_answers.iter() {
            let keys: HashSet<char> = one.keys().map(|k| *k).collect();
            all = all.intersection(&keys).map(|k| *k).collect();
        }

        // println!("{:?}: {:?}, {:?}:{:?}", group, &group_answers, all, all.len());
        counter += all.len();
    }
    println!("{:?}", counter);  // 3143
}
