use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use std::fs;

mod part1;
mod part2;

type Seating = String;

fn update_with_more_unique_chars(string: &String, unique_chars: &mut HashMap<char, i32>) {
    for char in string.chars().into_iter() {
        unique_chars.entry((&char).clone()).or_insert(1);
    }
}

pub fn day(filename: String) {
    let my_string = fs::read_to_string(filename).unwrap();
    let inputs: Vec<String> = my_string.split("\r\n\r\n").map(|s| s.to_string()).collect();

    println!("part 1:");
    part1::solve(&inputs); // 6351

    println!("part 2:");
    part2::solve(&inputs); // 3143
}
