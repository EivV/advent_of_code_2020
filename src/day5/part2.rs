//--- Part Two ---
//
// Ding! The "fasten seat belt" signs have turned on. Time to find your seat.
//
// It's a completely full flight, so your seat should be the only missing boarding pass in your
// list. However, there's a catch: some of the seats at the very front and back of the plane don't
// exist on this aircraft, so they'll be missing from your list as well.
//
// Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will
// be in your list.
//
// What is the ID of your seat?
use crate::day5::{Seating, narrow};
use std::str::Chars;

fn get_seat_id(max_row: i32, max_col: i32, seating: &Seating) -> i32 {
    let (row_seating, col_seating) = seating.split_at(7);
    let row = narrow(max_row, row_seating);
    let col = narrow(max_col, col_seating);
    let row_id = row * max_col + col;
    return row_id;
}

pub fn solve(inputs: &Vec<Seating>) {
    let mut taken = inputs.into_iter().map(|s| get_seat_id(128, 8, s)).collect::<Vec<i32>>();
    taken.sort();
    // println!("{:?}", taken);
    let mut current = &taken[0] - 1;

    let mut missing: Vec<i32> = vec![];
    for id in taken.into_iter() {
        if id - 1 == current {
            current = id;
            continue;
        }
        else {
            missing.push(current+1);
            break;
        }
    }
    println!("{:?}", current+1);  // 623
    // println!("missing: {:?}", missing);
}
