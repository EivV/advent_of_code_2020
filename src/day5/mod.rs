use std::fs;

mod part1;
mod part2;

type Seating = String;


fn narrow(range: i32, seating: &str) -> i32 {
    let mut minr = 0;
    let mut maxr = range - 1;
    for chr in seating.chars() {
        match chr {
            'B' | 'R' => {
                // Keep upper.
                // print!("{}: {:?}..{:?} -> ", chr, minr, maxr);
                maxr += 1;
                let mid = (maxr + minr) / 2;
                minr = mid;
                maxr -= 1;
                // println!("{:?}..{:?}", minr, maxr);
            }
            'F' | 'L' => {
                // Keep lower.
                // print!("{}: {:?}..{:?} -> ", chr, minr, maxr);
                maxr += 1;
                let mid = (maxr + minr) / 2;
                maxr = mid;
                maxr -= 1;
                // println!("{:?}..{:?}", minr, maxr);
            }
            _ => println!("BAD: {:?}", chr),
        }
    }
    assert_eq!(minr, maxr);
    return minr;
}


pub fn day(filename: String) {
    let my_string = fs::read_to_string(filename).unwrap();
    let inputs: Vec<String> = my_string.lines().map(|s| s.to_string()).collect();

    println!("part 1:");
    part1::solve(&inputs); // 885

    println!("part 2:");
    part2::solve(&inputs); // 623
}
