mod day1;
mod day2;
mod day3;
mod day5;
mod day6;

fn main() {
    let day = "6";
    let filename = format!("src/day{}/input", day,);
    println!("Day {}", day);
    match day {
        "1" => day1::day(filename),
        "2" => day2::day(filename),
        "3" => day3::day(filename),
        "5" => day5::day(filename),
        "6" => day6::day(filename),
        _ => println!("Unknown day {}", day),
    }
}
