use std::fs;

mod part1;
mod part2;

type WoodLine = String;

fn count_trees_on_slope(inputs: &&Vec<String>, h_step: usize, v_step: usize) -> i32 {
    let mut counter: i32 = 0;

    let total_lines: usize = inputs.len();
    let line_length: usize = inputs[0].len();

    let mut current_line: usize = 0;
    let mut current_vertical: usize = 0;

    let tree: char = "#".chars().next().unwrap();

    while current_line + h_step < total_lines {
        current_line += h_step;
        current_vertical = (current_vertical + v_step) % line_length;
        let wood_strip = &inputs[current_line];
        // println!("{:?}", wood_strip);
        let chr: char = wood_strip.chars().nth(current_vertical).unwrap();
        // println!("{:?}", chr);
        if chr == tree {
            counter += 1;
        }
    }
    counter
}


pub fn day(filename: String) {
    let my_string = fs::read_to_string(filename).unwrap();
    let inputs: Vec<String> = my_string.lines().map(|s| s.to_string()).collect();

    println!("part 1:");
    part1::solve(&inputs); // 193

    println!("part 2:");
    part2::solve(&inputs); // 1355323200
}
