//--- Part Two ---
//
// Time to check the rest of the slopes - you need to minimize the probability of a sudden arboreal
// stop, after all.
//
// Determine the number of trees you would encounter if, for each of the following slopes, you start
// at the top-left corner and traverse the map all the way to the bottom:
//
//     Right 1, down 1.
//     Right 3, down 1. (This is the slope you already checked.)
//     Right 5, down 1.
//     Right 7, down 1.
//     Right 1, down 2.
//
// In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied
// together, these produce the answer 336.
//
// What do you get if you multiply together the number of trees encountered on each of the listed
// slopes?
use crate::day3::{WoodLine, count_trees_on_slope};

pub fn solve(inputs: &Vec<WoodLine>) {
    let slopes: [(usize, usize); 5] = [
        (1, 1),
        (1, 3),
        (1, 5),
        (1, 7),
        (2, 1),
    ];

    let mut counter = 1;
    for (h_step, v_step) in slopes.iter() {
        // println!("slope: {}, {}", h_step, v_step);
        let count = count_trees_on_slope(&inputs, *h_step, *v_step);
        // println!("count: {}", count);
        counter *= count;
    }

    println!("{:?}", counter); // 1355323200
}
