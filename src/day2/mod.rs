use std::fs;
use std::str::FromStr;

mod part1;
mod part2;

#[derive(Debug)]
pub struct Input {
    from: i32,
    to: i32,
    letter: String,
    password: String,
}

impl FromStr for Input {
    type Err = std::num::ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let the_split: Vec<&str> = input.split(':').collect();

        let left: &str = the_split[0];
        let right: &str = the_split[1];

        let range_and_letter: Vec<String> = left.split_whitespace().map(String::from).collect();

        let range: Vec<&str> = range_and_letter[0]
            .split_whitespace()
            .map(|s| s.split('-'))
            .into_iter()
            .flatten()
            .collect::<Vec<_>>();

        let from = range[0].parse::<i32>().unwrap();
        let to = range[1].parse::<i32>().unwrap();
        let letter: String = range_and_letter[1].to_string();
        let password: String = right
            .chars()
            .filter(|c| !c.is_whitespace())
            .collect::<String>();

        Ok(Input {
            from,
            to,
            letter,
            password,
        })
    }
}

pub fn day(filename: String) {
    let my_string = fs::read_to_string(filename).unwrap();
    let numbers: Vec<&str> = my_string.lines().collect();

    let inputs: Vec<Input> = numbers
        .into_iter()
        .map(|a| Input::from_str(a).unwrap())
        .collect();

    println!("part 1:");
    part1::solve(&inputs); // 666

    println!("part 2:");
    part2::solve(&inputs); // 670
}
